const http = require('http');

const port = 3000;

const server = http.createServer(function (request, response) {

	if(request.url == '/greeting') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Page successfully running')
	} else if (request.url == '/loginpage') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the Login page')
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page not found.')
	}
});

server.listen(port); 

console.log(`Server is now accessible at localhost: ${port}`);